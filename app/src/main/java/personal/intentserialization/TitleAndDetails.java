package personal.intentserialization;

import java.io.Serializable;

class TitleAndDetails implements Serializable{
    private String title;
    private String details;

    protected TitleAndDetails setTitle(String title) {
        this.title = title;
        return this;
    }

    protected TitleAndDetails setDetails(String details) {
        this.details = details;
        return this;
    }

    protected String getTitle() {
        return this.title;
    }

    protected String getDetails() {
        return this.details;
    }
}
