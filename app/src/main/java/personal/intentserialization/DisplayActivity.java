package personal.intentserialization;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;

import java.io.Serializable;

public class DisplayActivity extends AppCompatActivity {
    private TextView textViewTitle;
    private TextView textViewDetails;
    private Intent intent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_display);
        this.init();
    }



    private void init() {
        this.textViewTitle = (TextView) this.findViewById(R.id.display_text_view_title);
        this.textViewDetails = (TextView) this.findViewById(R.id.display_text_view_details);
        this.intent = this.getIntent();

        Serializable serializableExtra = this.intent.getSerializableExtra(MainActivity.INTENT_TITLEANDDETAILS_NAME);
        TitleAndDetails titleAndDetails = (TitleAndDetails) serializableExtra;
        this.textViewTitle.setText(titleAndDetails.getTitle());
        this.textViewDetails.setText(titleAndDetails.getDetails());
    }

}
