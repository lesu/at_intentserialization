package personal.intentserialization;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
    private EditText editTextTitle;
    private EditText editTextDetails;
    private Button buttonGo;
    static final String INTENT_TITLEANDDETAILS_NAME = "title_details";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        this.init();
    }


    private void init() {
        this.editTextTitle = (EditText) this.findViewById(R.id.main_edit_text_title);
        this.editTextDetails = (EditText) this.findViewById(R.id.main_edit_text_details);
        this.buttonGo = (Button) this.findViewById(R.id.main_button_go);

        this.buttonGo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String title = MainActivity.this.editTextTitle.getText().toString();
                String details = MainActivity.this.editTextDetails.getText().toString();
                title = (title==null) ? "" : title;
                details = (details==null) ? "" : details;
                if( (title.equals(""))  ||  (details.equals(""))    ) {
                    Toast.makeText(MainActivity.this,"Invalid input",Toast.LENGTH_SHORT).show();
                    return;
                }
                MainActivity.this.editTextTitle.setText("");
                MainActivity.this.editTextDetails.setText("");

                TitleAndDetails titleAndDetails = new TitleAndDetails().setTitle(title).setDetails(details);
                MainActivity.this.startActivity(new Intent(MainActivity.this,DisplayActivity.class).putExtra(MainActivity.INTENT_TITLEANDDETAILS_NAME,titleAndDetails));
            }
        });


    }

}
